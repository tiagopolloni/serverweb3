#!/bin/bash

# Update hosts file
echo "[TASK 1] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
172.20.0.1    invictus.local
EOF

# Disable SELinux
echo "[TASK 2] Disabling SELinux"
setenforce 0
sed -i 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/sysconfig/selinux

# Stop and disable firewalld
echo "[TASK 3] Stopping and Disabling firewalld"
systemctl disable firewalld >/dev/null 2>&1
systemctl stop firewalld

# Install essentials packages
echo "[TASK 4] Installing essentials packages"
yum install -y vim wget net-tools unzip telnet ntpdate >/dev/null 2>&1
