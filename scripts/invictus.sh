#!/bin/bash

# Install httpd
echo "[TASK 1] Installing httpd"
yum install -y httpd >/dev/null 2>&1
systemctl start httpd >/dev/null 2>&1

# Create file
echo "[TASK 2] Creating html file"
mkdir /var/www/invictus
cat <<'EOF' >> /var/www/invictus/index.html
<!DOCTYPE html>
<html>
    <head>
        <title>httpd test</title>
    </head>
    <body>
        <h1>HTTPD TESTE DE SITE</h1>
        <p>httpd test aoww
        </p>
    </body>
</html>
EOF

# Configure virtual host
echo "[TASK 3] Configuring Virtual Host"
cat <<'EOF' >> /etc/httpd/conf.d/001-invictus.conf
<VirtualHost *:80>
    ServerName invictus.test
    DocumentRoot /var/www/invictus
    CustomLog /var/log/httpd/invictus-access.log combined
    ErrorLog /var/log/httpd/invictus-error.log
</VirtualHost>
EOF

# Restart httpd
echo "[TASK 4] Restarting httpd"
systemctl restart httpd >/dev/null 2>&1

# Determine IP Address
echo "ip address:" && hostname -I | awk '{ print $3 }'

